import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TodoListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Todo List")), body: _buildBody(context));
  }

  final TextEditingController _controller = TextEditingController();

  _buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(children: [
        Row(
          children: [
            Expanded(
                child: TextField(
              controller: _controller,
              decoration: InputDecoration(hintText: "Enter task name"),
            )),
            TextButton(
                style: flatButtonStyle,
                onPressed: () {
                  _addTask();
                },
                child: Text("Add Task"))
          ],
        ),

        StreamBuilder<QuerySnapshot>(
            stream: FirebaseFirestore.instance.collection('todos').snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return LinearProgressIndicator();

              return Expanded(child: _buildList(snapshot.data));
            })

        // Expanded(
        //     child: ListView.builder(
        //         itemCount: 20,
        //         itemBuilder: (context, index) {
        //           return ListTile(title: Text("Your task here ..."));
        //         }))
      ]),
    );
  }

  Widget _buildList(QuerySnapshot snapshot) {
    return ListView.builder(
        itemCount: snapshot.docs.length,
        itemBuilder: (context, index) {
          final doc = snapshot.docs[index];

          return _buildListItem(doc['title'], doc.id);
          //return ListTile(title: Text(doc['title']));
        });
  }

  Widget _buildListItem(String title, String id) {
    return Dismissible(
        key: Key(id),
        onDismissed: (direction) {
          _deleteTask(title, id);
        },
        background: Container(color: Colors.red),
        child: ListTile(
          title: Text(title),
        ));
  }

  _deleteTask(String title, String id) async {
    await FirebaseFirestore.instance.collection("todos").doc(id).delete();
  }

  void _addTask() {
    FirebaseFirestore.instance
        .collection("todos")
        .add({"title": _controller.text});

    _controller.text = "";
  }

  final ButtonStyle flatButtonStyle = TextButton.styleFrom(
    primary: Colors.white,
    minimumSize: Size(88, 44),
    padding: EdgeInsets.symmetric(horizontal: 16.0),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(2.0)),
    ),
    backgroundColor: Colors.blue,
  );
}
