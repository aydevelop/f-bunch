import 'package:cloud_firestore/cloud_firestore.dart';

class Task {
  final String title;
  final String taskId;

  Task(this.title, [this.taskId]);

  factory Task.fromSnapshot(DocumentSnapshot snapshot) {
    return Task(snapshot["title"], snapshot.id);
  }
}
