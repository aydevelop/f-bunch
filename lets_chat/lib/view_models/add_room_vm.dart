import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:lets_chat/models/rooom.dart';

class AddRoomVM extends ChangeNotifier {
  Future<bool> addRoom(String title, String description) async {
    String message = "";
    final room = Room(title: title, description: description);
    bool isSaved = false;

    try {
      await FirebaseFirestore.instance.collection("rooms").add(room.toMap());
      isSaved = true;
    } on FirebaseException catch (e) {
      message = "Error adding a room";
      print(e);
    } catch (e) {
      message = e.code;
      print(e);
    }

    notifyListeners();
    return isSaved;
  }
}
