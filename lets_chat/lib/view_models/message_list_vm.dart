import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lets_chat/models/message.dart';
import 'package:lets_chat/models/rooom.dart';

class MessageListVM {
  final Room room;
  final messagesStream = StreamController<List<Message>>();

  MessageListVM(this.room) {
    _subscribeToMessagesStrem();
  }

  // Stream<QuerySnapshot> get storeItemsAsStream {
  //       return FirebaseFirestore.instance
  //       .collection("stores")
  //       .doc(store.storeId)
  //       .collection("items")
  //       .snapshots();
  // }

  void _subscribeToMessagesStrem() {
    FirebaseFirestore.instance
        .collection("messages")
        // .where("roomId", isEqualTo: room.roomId)
        // .orderBy("dateCreated", descending: false)
        .snapshots()
        .listen((event) {
      final messages = event.docs.map((doc) => Message.fromDoc(doc)).toList();
      messagesStream.sink.add(messages);
    });
  }

  Future<bool> sendMessage(
      String roomId, String messageText, String username) async {
    final msg = Message(
        roomId: roomId,
        messageText: messageText,
        username: username,
        dateCreated: DateTime.now());

    bool isSaved = false;
    try {
      await FirebaseFirestore.instance.collection("messages").add(msg.toMap());
      isSaved = true;
    } catch (e) {
      print(e);
    }

    return isSaved;
  }
}
