import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lets_chat/models/rooom.dart';

class RoomListVM {
  Future<List<Room>> getAllRooms() async {
    final QuerySnapshot snapshot =
        await FirebaseFirestore.instance.collection("rooms").get();
    final rooms = snapshot.docs.map((doc) => Room.fromDocument(doc)).toList();
    return rooms;
  }
}
