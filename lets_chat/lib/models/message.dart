import 'package:cloud_firestore/cloud_firestore.dart';

class Message {
  final String messageId;
  final String roomId;
  final String messageText;
  final String username;
  final DateTime dateCreated;

  Message(
      {this.messageId,
      this.roomId,
      this.messageText,
      this.username,
      this.dateCreated});

  Map<String, dynamic> toMap() {
    return {
      "messageId": messageId,
      "roomId": roomId,
      "messageText": messageText,
      "username": username,
      "dateCreated": dateCreated
    };
  }

  factory Message.fromDoc(QueryDocumentSnapshot doc) {
    return Message(
        messageId: doc.id,
        roomId: doc["roomId"],
        messageText: doc["messageText"],
        username: doc["username"],
        dateCreated: doc["dateCreated"].to);
  }
}
