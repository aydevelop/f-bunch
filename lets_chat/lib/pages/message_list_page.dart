import 'package:flutter/material.dart';
import 'package:lets_chat/models/message.dart';
import 'package:lets_chat/models/rooom.dart';
import 'package:lets_chat/view_models/message_list_vm.dart';
import 'package:lets_chat/widgets/message_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MessageListPage extends StatefulWidget {
  final Room room;
  const MessageListPage({this.room});

  @override
  _MessageListPage createState() => _MessageListPage();
}

class _MessageListPage extends State<MessageListPage> {
  MessageListVM _messageListVM;
  final _scrollController = ScrollController();
  final _messageTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _messageListVM = MessageListVM(widget.room);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _sendMessage(BuildContext context) async {
    final messageText = _messageTextController.text;
    final sharedPreferences = await SharedPreferences.getInstance();
    final username = sharedPreferences.get("username");

    if (messageText.isNotEmpty) {
      _messageTextController.clear();

      await _messageListVM.sendMessage(
          widget.room.roomId, messageText, username);
    }
  }

  Widget _buildMessageList(List<Message> messages) {
    return Expanded(
      child: ListView.builder(
          itemCount: messages.length,
          itemBuilder: (context, index) {
            final message = messages[index];
            return MessageWidget(
                messageText: message.messageText,
                senderUserName: message.username);
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(widget.room.title)),
        body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          StreamBuilder(
              stream: _messageListVM.messagesStream.stream,
              builder: (context, spanshot) {
                if (spanshot.hasError) {
                  return Text("Error: ${spanshot.error}");
                }

                switch (spanshot.connectionState) {
                  case ConnectionState.waiting:
                    return const Text("Loading...");
                  default:
                    if (!spanshot.hasData) {
                      return Text("No Data");
                    }

                    return _buildMessageList(spanshot.data);
                }
              }),
          Spacer(),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: _messageTextController,
                      decoration: InputDecoration(hintText: "Enter message"),
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.send),
                    color: Colors.green,
                    onPressed: () {
                      _sendMessage(context);
                    },
                  )
                ],
              ),
            ),
          )
        ]));
  }
}
