import 'package:flutter/material.dart';
import 'package:lets_chat/pages/main_page.dart';
import 'package:lets_chat/view_models/login_vm.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPage createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(Image.asset("assets/images/logo.png").image, context);
  }

  String _message = "";

  final _usernameController = TextEditingController();
  final _loginVM = LoginVM();

  @override
  void initState() {
    super.initState();
    _usernameController.text = "root";
  }

  void _login(BuildContext context) async {
    final username = _usernameController.text;

    if (username.isEmpty) {
      setState(() {
        _message = "Username cannot be empty!";
      });
    } else {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => MainPage()));

      final isLoggedIn = await _loginVM.login(username);
      if (isLoggedIn) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => MainPage()));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: CircleAvatar(
              maxRadius: 150,
              backgroundImage: AssetImage("assets/images/logo.png")),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            controller: _usernameController,
            decoration: InputDecoration(hintText: "Enter username"),
          ),
        ),
        FlatButton(
            child: Text("Login"),
            onPressed: () {
              _login(context);
            },
            textColor: Colors.white,
            color: Colors.purple),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(_message),
        )
      ]),
    ));
  }
}
