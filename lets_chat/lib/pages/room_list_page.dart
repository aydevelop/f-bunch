import 'package:flutter/material.dart';
import 'package:lets_chat/models/rooom.dart';
import 'package:lets_chat/pages/add_room_page.dart';
import 'package:lets_chat/pages/message_list_page.dart';
import 'package:lets_chat/view_models/add_room_vm.dart';
import 'package:lets_chat/view_models/room_list_vm.dart';
import 'package:lets_chat/widgets/room_list_widget.dart';
import 'package:provider/provider.dart';

class RoomListPage extends StatefulWidget {
  @override
  _RoomListPage createState() => _RoomListPage();
}

class _RoomListPage extends State<RoomListPage> {
  RoomListVM _roomListVM = RoomListVM();
  List<Room> _rooms = [];

  @override
  void initState() {
    super.initState();
    _populateRooms();
  }

  void _populateRooms() async {
    final rooms = await _roomListVM.getAllRooms();
    setState(() {
      _rooms = rooms;
    });
  }

  void _navigateToAddRoomPage(BuildContext context) async {
    bool roomAdded = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                  create: (context) => AddRoomVM(),
                  child: AddRoomPage(),
                ),
            fullscreenDialog: true));

    if (roomAdded != null && roomAdded) {
      _populateRooms();
    }
  }

  void _navigateToMsgByRoom(Room room, BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MessageListPage(room: room),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Rooms"),
          actions: [
            GestureDetector(
                onTap: () {
                  _navigateToAddRoomPage(context);
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Icon(Icons.add, size: 32),
                ))
          ],
        ),
        body: RoomListWidget(
            rooms: _rooms,
            onRoomSelected: (room) {
              _navigateToMsgByRoom(room, context);
            }));
  }
}
