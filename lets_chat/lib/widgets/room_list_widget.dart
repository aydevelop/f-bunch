import 'package:flutter/material.dart';
import 'package:lets_chat/models/rooom.dart';

class RoomListWidget extends StatelessWidget {
  final List<Room> rooms;
  final Function(Room) onRoomSelected;

  const RoomListWidget({this.rooms, this.onRoomSelected});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: rooms.length,
        itemBuilder: (context, index) {
          final room = rooms[index];

          return ListTile(
            title: Text(room.title),
            trailing: Icon(Icons.arrow_forward_ios),
            subtitle: Text(room.description),
            onTap: () {
              onRoomSelected(room);
            },
          );
        });
  }
}
