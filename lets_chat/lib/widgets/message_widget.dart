import 'package:flutter/material.dart';

class MessageWidget extends StatefulWidget {
  final String messageText;
  final String senderUserName;
  String currentUserName;

  MessageWidget({this.messageText, this.senderUserName});

  @override
  _MessageWidget createState() => _MessageWidget();
}

class _MessageWidget extends State<MessageWidget> {
  String _currentUserName;
  bool _messageByCurrentUser = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Flex(
        direction: Axis.horizontal,
        mainAxisAlignment: _messageByCurrentUser
            ? MainAxisAlignment.end
            : MainAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * 0.7),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(widget.messageText,
                      style: TextStyle(color: Colors.white)),
                ),
                color: _messageByCurrentUser ? Colors.green : Colors.grey),
          )
        ],
      ),
    );
  }
}
