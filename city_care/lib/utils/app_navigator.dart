import 'package:city_care/pages/add_incidents_page.dart';
import 'package:city_care/pages/login_page.dart';
import 'package:city_care/pages/register_page.dart';
import 'package:city_care/view_models/add_incidents_vm.dart';
import 'package:city_care/view_models/login_vm.dart';
import 'package:city_care/view_models/register_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AppNavigator {
  static Future<bool> navigateToLoginPage(BuildContext context) async {
    return await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => LoginVM(), child: LoginPage()),
            fullscreenDialog: true));
  }

  static Future<bool> navigateToRegisterPage(BuildContext context) async {
    return await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => RegisterVM(), child: RegisterPage()),
            fullscreenDialog: true));
  }

  static Future<bool> navigateToAddIncidentsPage(BuildContext context) async {
    // Navigator.push(
    //     context, MaterialPageRoute(builder: (context) => AddIncidentsPage()));

    return await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => AddIncidentVM(),
                child: AddIncidentsPage()),
            fullscreenDialog: true));
  }
}
