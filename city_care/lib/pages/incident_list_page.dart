import 'package:city_care/models/incident.dart';
import 'package:city_care/pages/add_incidents_page.dart';
import 'package:city_care/pages/login_page.dart';
import 'package:city_care/pages/my_incidents_page.dart';
import 'package:city_care/pages/register_page.dart';
import 'package:city_care/utils/app_navigator.dart';
import 'package:city_care/view_models/incident_list_vm.dart';
import 'package:city_care/view_models/login_vm.dart';
import 'package:city_care/view_models/register_vm.dart';
import 'package:city_care/widgets/empty_or_no_items_widget.dart';
import 'package:city_care/widgets/incident_list_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class IncidentListPage extends StatefulWidget {
  @override
  _IncidentListPage createState() => _IncidentListPage();
}

class _IncidentListPage extends State<IncidentListPage> {
  bool _isSignedIn = false;
  IncidentListVM _incidentListVM = IncidentListVM();
  List<Incident> _incidents = [];

  @override
  void initState() {
    super.initState();
    _subscribeToFirebaseAuthChanges();
    _populateAllIncidents();
  }

  void _subscribeToFirebaseAuthChanges() {
    FirebaseAuth.instance.authStateChanges().listen((user) {
      if (user == null) {
        setState(() {
          _isSignedIn = false;
        });
      } else {
        setState(() {
          _isSignedIn = true;
        });
      }
    });
  }

  void _populateAllIncidents() async {
    final incidents = await _incidentListVM.getAllIncidents();

    setState(() {
      _incidents = incidents;
    });
  }

  void _navigateToRegisterPage(BuildContext context) async {
    final isReg = await AppNavigator.navigateToRegisterPage(context);

    if (isReg) {
      await AppNavigator.navigateToLoginPage(context);
      //Navigator.of(context).pop();
    }
  }

  void _navigateToLoginPage(BuildContext context) async {
    await AppNavigator.navigateToLoginPage(context);
    //Navigator.of(context).pop();
  }

  void _navigateToMyIncidentsPage(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => MyIncidentsPage()));
  }

  void _navigateToAddIncidentsPage(BuildContext context) async {
    bool incidentAdded = await AppNavigator.navigateToAddIncidentsPage(context);
    if (incidentAdded) {
      _populateAllIncidents();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Latest Incidents")),
        drawer: Drawer(
            child: ListView(
          children: [
            SizedBox(
              width: 0.0,
              height: 30.0,
            ),
            ListTile(
                title: Text("Home"),
                onTap: () {
                  Navigator.of(context).pop();
                }),
            // ListTile(
            //     title: Text("My Incidents"),
            //     onTap: () async {
            //       _navigateToMyIncidentsPage(context);
            //     }),
            _isSignedIn
                ? ListTile(
                    title: Text("Add Incident"),
                    onTap: () {
                      _navigateToAddIncidentsPage(context);
                    },
                  )
                : SizedBox.shrink(),
            !_isSignedIn
                ? ListTile(
                    title: Text("Login"),
                    onTap: () {
                      _navigateToLoginPage(context);
                    })
                : SizedBox.shrink(),
            !_isSignedIn
                ? ListTile(
                    title: Text("Register"),
                    onTap: () {
                      _navigateToRegisterPage(context);
                    })
                : SizedBox.shrink(),
            _isSignedIn
                ? ListTile(
                    title: Text("Logout"),
                    onTap: () async {
                      await FirebaseAuth.instance.signOut();
                    })
                : SizedBox.shrink()
          ],
        )),
        body: Padding(
          padding: const EdgeInsets.all(18.0),
          child: (_incidents.length != 0
              ? IncidentListWidget(_incidents)
              : EmptyOrNoItemsWidget(message: "No incidents found")),
        ));
  }
}
