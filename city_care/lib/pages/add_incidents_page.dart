import 'dart:io';
import 'package:city_care/models/incident.dart';
import 'package:city_care/view_models/add_incidents_vm.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
//import 'package:image_picker/image_picker.dart';

enum PhotoOptions { camera, library }

class AddIncidentsPage extends StatefulWidget {
  @override
  _AddIncidentsPage createState() => _AddIncidentsPage();
}

class _AddIncidentsPage extends State<AddIncidentsPage> {
  File _image;
  final _formKey = GlobalKey<FormState>();
  AddIncidentVM _addIncidentVM;

  final _titleController = TextEditingController();
  final _descriptionController = TextEditingController();

  void _selectPhotoFromPhotoLibrary() async {
    final imgPicker = ImagePicker();
    final pickedFile = await imgPicker.getImage(source: ImageSource.gallery);

    setState(() {
      _image = File(pickedFile.path);
      print("_image: " + _image.path);
    });
  }

  void _selectPhotoFromCamera() async {
    final imgPicker = ImagePicker();
    final pickedFile = await imgPicker.getImage(source: ImageSource.camera);

    setState(() {
      _image = File(pickedFile.path);
      print("_image2: " + _image.path);
    });
  }

  void _optionSelected(PhotoOptions option) {
    switch (option) {
      case PhotoOptions.camera:
        _selectPhotoFromCamera();
        break;
      case PhotoOptions.library:
        _selectPhotoFromPhotoLibrary();
        break;
    }
  }

  void _saveIncident(BuildContext context) async {
    // validate the form
    if (_formKey.currentState.validate()) {
      final filePath = await _addIncidentVM.uploadFile(_image);
      if (filePath.isNotEmpty) {
        final title = _titleController.text;
        final description = _descriptionController.text;

        final userId = FirebaseAuth.instance.currentUser.uid;
        final incident = Incident(
            userId: userId,
            title: title,
            description: description,
            photoURL: filePath,
            incidentDate: DateTime.now());

        final isSaved = await _addIncidentVM.saveIncident(incident);
        if (isSaved) {
          Navigator.pop(context, true);
        }
      }
    }
  }

  Widget _buildLoadingWidget() {
    return Text("");
  }

  @override
  Widget build(BuildContext context) {
    _addIncidentVM = Provider.of<AddIncidentVM>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text("Add Incident"),
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(children: [
                  SizedBox(
                      child: _image == null
                          ? Image.asset("assets/images/city_care.jpg")
                          : Image.file(_image),
                      width: 200,
                      height: 200),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: PopupMenuButton<PhotoOptions>(
                      // ignore: missing_required_param
                      child: TextButton(
                          style: flatButtonStyle,
                          child: Text("Add Incident",
                              style: TextStyle(color: Colors.white))),
                      onSelected: _optionSelected,
                      itemBuilder: (context) => [
                        PopupMenuItem(
                          child: Text("Take a picture"),
                          value: PhotoOptions.camera,
                        ),
                        PopupMenuItem(
                            child: Text("Select from photo library"),
                            value: PhotoOptions.library)
                      ],
                    ),
                  ),
                  TextFormField(
                    controller: _titleController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Title is required!";
                      }
                      return null;
                    },
                    decoration:
                        InputDecoration(hintText: "Enter incident title"),
                  ),
                  TextFormField(
                    controller: _descriptionController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Description is required!";
                      }
                      return null;
                    },
                    maxLines: null,
                    decoration:
                        InputDecoration(hintText: "Enter incident description"),
                  ),
                  FlatButton(
                    child: Text("Submit"),
                    onPressed: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      _saveIncident(context);
                    },
                    color: Colors.blue,
                    textColor: Colors.white,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(_addIncidentVM.message),
                  ),
                  _buildLoadingWidget()
                ]),
              ),
            ),
          ),
        ));
  }
}

// Widget MyFutureBuilder(String path) {
//   return new FutureBuilder(future: () {
//     File f = new File(path);
//     return f;
//   }, builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
//     return snapshot.data != null
//         ? new Image.file(snapshot.data)
//         : new Container();
//   });
// }

// Widget MyFutureBuilderLocal(String path) {
//   return new FutureBuilder(
//       future: _getLocalFile(path),
//       builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
//         return snapshot.data != null
//             ? new Image.file(snapshot.data)
//             : new Container();
//       });
// }

Future<File> _getLocalFile(String filename) async {
  String dir = (await getApplicationDocumentsDirectory()).path;
  File f = new File('$dir/$filename');
  return f;
}

final ButtonStyle flatButtonStyle = TextButton.styleFrom(
  primary: Colors.white,
  minimumSize: Size(88, 44),
  padding: EdgeInsets.symmetric(horizontal: 16.0),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(2.0)),
  ),
  backgroundColor: Colors.red,
);
