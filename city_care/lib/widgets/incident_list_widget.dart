import 'package:city_care/models/incident.dart';
import 'package:flutter/material.dart';

class IncidentListWidget extends StatelessWidget {
  List<Incident> _incidents = [];

  IncidentListWidget(List<Incident> incidents) {
    this._incidents = incidents;
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _incidents.length,
      itemBuilder: (context, index) {
        final incident = _incidents[index];

        return ListTile(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.network(incident.photoURL, fit: BoxFit.cover),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(incident.title,
                    style:
                        TextStyle(fontWeight: FontWeight.w800, fontSize: 18)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(incident.incidentDateFormat(incident.incidentDate),
                    style: TextStyle(
                        fontStyle: FontStyle.italic,
                        fontSize: 12,
                        fontWeight: FontWeight.w400)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(""),
              )
            ],
          ),
        );
      },
    );
  }
}
