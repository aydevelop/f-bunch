import 'package:flutter/material.dart';

class EmptyOrNoItemsWidget extends StatelessWidget {
  final String message;

  EmptyOrNoItemsWidget({this.message = ""});

  @override
  Widget build(BuildContext context) {
    return Text(message, style: TextStyle(fontSize: 18));
  }
}
