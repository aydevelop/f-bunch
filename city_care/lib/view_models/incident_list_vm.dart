import 'package:city_care/models/incident.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class IncidentListVM extends ChangeNotifier {
  String message = "";

  Future<List<Incident>> getAllIncidents() async {
    //DateFormat("MM-dd-yyyy HH:mm:ss").format(incident.incidentDate);
    // final QuerySnapshot snapshot =
    //     await FirebaseFirestore.instance.collection("incidents").orderBy("incident");
    //return DateFormat("MM-dd-yyy HH:mm:ss").

    // final QuerySnapshot snapshot = await FirebaseFirestore.instance.collection("incidents")
    //   .where("userId", isEqualTo: userId)
    //   .orderBy("incidentDate", descending: true)
    //   .get();

    final QuerySnapshot snapshot = await FirebaseFirestore.instance
        .collection("incidents")
        .orderBy("incidentDate", descending: true)
        .get();

    final incidents =
        snapshot.docs.map((doc) => Incident.fromDocument(doc)).toList();

    return incidents;
  }
}
