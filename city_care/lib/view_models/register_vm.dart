import 'package:city_care/utils/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class RegisterVM extends ChangeNotifier {
  String message = "";

  Future<bool> register(String email, String password) async {
    bool isRegistered = false;

    try {
      final userSome = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      isRegistered = userSome != null;
    } on FirebaseAuthException catch (e) {
      if (e.code == "weak-password") {
        message = Constants.WEAK_PASSWORD;
      } else if (e.code == "email-already-in-use") {
        message = Constants.EMAIL_ALREADY_IN_USE;
      } else {
        message = "error: " + e.message;
      }
    } catch (e) {
      print("ERROR: " + e);
    }

    notifyListeners();
    return isRegistered;
  }
}
