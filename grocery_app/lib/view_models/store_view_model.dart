import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:grocery_app/models/store.dart';

class StoreViewModel {
  final Store store;
  StoreViewModel({this.store});

  String get storeId {
    return store.storeId;
  }

  String get name {
    return store.name;
  }

  String get address {
    return store.address;
  }

  Future<int> get itemsCount async {
    final data = await store.reference.collection('items').get();
    return data.docs.length;
  }

  factory StoreViewModel.fromSnapsShot(QueryDocumentSnapshot doc) {
    final store = Store(doc["name"], doc["address"], doc.reference);
    return StoreViewModel(store: store);
  }
}
