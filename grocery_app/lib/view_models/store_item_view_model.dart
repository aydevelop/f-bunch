import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:grocery_app/models/store_item.dart';

class StoreItemViewMode {
  final StoreItem storeItem;

  StoreItemViewMode({this.storeItem});

  String get name {
    return storeItem.name;
  }

  String get storeItemId {
    return storeItem.storeItemId;
  }

  factory StoreItemViewMode.fromSnapshot(QueryDocumentSnapshot doc) {
    final storeItem = StoreItem.fromSnapshot(doc);
    return StoreItemViewMode(storeItem: storeItem);
  }
}
