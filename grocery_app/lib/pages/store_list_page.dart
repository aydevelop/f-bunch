import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:grocery_app/pages/add_store_page.dart';
import 'package:grocery_app/pages/store_item_list_page.dart';
import 'package:grocery_app/utils/constants.dart';
import 'package:grocery_app/view_models/add_store_view_model.dart';
import 'package:grocery_app/view_models/store_list_view_model.dart';
import 'package:grocery_app/view_models/store_view_model.dart';
import 'package:grocery_app/widgets/empty_results_widget.dart';
import 'package:provider/provider.dart';

class StoreListPage extends StatefulWidget {
  @override
  _StoreListPage createState() => _StoreListPage();
}

class _StoreListPage extends State<StoreListPage> {
  StoreListViewModel _storeListVM = StoreListViewModel();

  Widget _buildBody() {
    return StreamBuilder<QuerySnapshot>(
        stream: _storeListVM.storesAsStream,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data.docs.isNotEmpty) {
            return _buildList(snapshot.data);
          } else {
            return EmptyResultsWidget(message: Constants.NO_STORES_FOUND);
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Grocery App"),
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: GestureDetector(
                child: Icon(Icons.add),
                onTap: () {
                  _navigateToAddStorePage(context);
                },
              ),
            )
          ],
        ),
        body: _buildBody());
  }
}

Widget _buildList(QuerySnapshot snapshot) {
  final stores =
      snapshot.docs.map((doc) => StoreViewModel.fromSnapsShot(doc)).toList();

  return ListView.builder(
    itemCount: stores.length,
    itemBuilder: (context, index) {
      final store = stores[index];
      return _buildListItem(store, (store) {
        _navigateToStoreItems(context, store);
      });
    },
  );
}

void _navigateToStoreItems(BuildContext context, StoreViewModel store) {
  Navigator.push(context,
      MaterialPageRoute(builder: (context) => StoreItemListPage(store: store)));
}

Widget _buildListItem(
    StoreViewModel store, void Function(StoreViewModel) onStoreSelected) {
  return ListTile(
      title: Text(store.name,
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
      subtitle: Text(store.address),
      // trailing: SizedBox(
      //   width: 100,
      //   child: Row(
      //     children: [
      //       Text("10011"),
      //       Spacer(),
      //       Icon(Icons.arrow_forward_ios),
      //     ],
      //   ),
      // ),
      trailing: FutureBuilder<int>(
          future: store.itemsCount,
          builder: (context, snapshot) {
            String count = "";
            if (snapshot.hasData) {
              count = snapshot.data.toString();
            } else {
              count = "0";
            }

            return SizedBox(
              width: 80,
              child: Row(
                children: [
                  _drawCircle(count),
                  Spacer(),
                  Icon(Icons.arrow_forward_ios),
                ],
              ),
            );
          }),
      onTap: () {
        onStoreSelected(store);
      });
}

Widget _drawCircle(String count) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(20),
    child: Container(
      alignment: Alignment.center,
      child: Text(count, style: TextStyle(color: Colors.white, fontSize: 14)),
      color: Colors.black,
      width: 25,
      height: 25,
    ),
  );
}

void _navigateToAddStorePage(BuildContext context) {
  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ChangeNotifierProvider(
              create: (context) => AddStoreViewModel(), child: AddStorePage()),
          fullscreenDialog: true));
}
